"use strict";

module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.bulkInsert(
      "Students",
      [
        {
          name: "John Doe",
          age: 30,
          datebirth:4/15/96,
          registrationdate: 12/10/21,
          cost: 150,
          createdAt: new Date(),
          updatedAt: new Date(),
          
        },
        {
          name: "Mary Doe",
          age: 30,
          datebirth: 4/15/96,
          registrationdate: 12/10/21,
          cost: 150,
          createdAt: new Date(),
          updatedAt: new Date(),
        }
      ],
      {}
    );
  },

  async down(queryInterface, Sequelize) {
    await queryInterface.bulkDelete("Students", null, {});
  },
};
