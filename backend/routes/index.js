const express = require('express')
const router = express.Router()
const path = require('path')

const {create,update, remove, getAll,getById} = require('../controllers/index')

router.get("/",getAll)
router.post("/newStudent", create)
router.get("/modificar/:id", update)
router.delete("/borrar",remove)
router.get("/student/:id",getById )

module.exports= router